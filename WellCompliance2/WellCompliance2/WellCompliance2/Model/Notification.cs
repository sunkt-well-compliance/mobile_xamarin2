﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WellCompliance2.Model
{
    class Notification
    {
        /* Модалка с оповещением */
        public async void DisplayMessage(string title, string text, string textbutton)
        {
            App.Current.MainPage.DisplayAlert(title, text, textbutton);
        }
    }
}
