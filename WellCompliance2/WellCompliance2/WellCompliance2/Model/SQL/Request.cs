﻿using System;
using System.IO;
using SQLite;
using Xamarin.Essentials;


namespace WellCompliance2.Model.SQL
{
    class Request
    {
        string dbFileName = Path.Combine(FileSystem.AppDataDirectory, "wcbase.db3"); // Полный путь к файлу базы
                                                                                     //  string dbFileName = Path.Combine("wcbase.doc"); // FILE NAME TO USE WHEN COPIED
        /* Читаем файл базы. Если файла базы нет, то мы его создаем */

        public SQLiteConnection BD()
        {
            if (!File.Exists(dbFileName))
            {
                File.Create(dbFileName);
            }

            SQLiteConnection connection = new SQLiteConnection(dbFileName);
            return connection;
        }

        /* Проверка существования базы */
        public bool ChekExistBD()
        {
            if (File.Exists(dbFileName))
            {
                return true;

            }
            else
            {
                return false;
            }
        }

        /* Создание базы */
        public bool CreateBD()
        {

            try
            {
                File.Create(dbFileName);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
