﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace WellCompliance2.Model.SQL
{
    class Base
    {
    }

    /* Профиль текущего пользователя */
    [Table("Profile")]
    public class Profile
    {
        public int Id { get; set; } 
        public string Login { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string FatherName { get; set; }
        public string Email { get; set; }
        public string Platforms { get; set; }
        public int Status { get; set; }
        public int Role { get; set; }
    }

    /* Настройки */
    [Table("Settings")]
    public class Settings
    {
        public string Server { get; set; } // URL сервера
        public bool EnableSaveMe { get; set; } // Кнопка "Сохранить меня"
        public bool EnableModuleNM { get; set; } // Включен модуль "NearMiss"?
        public bool EnableModuleTask { get; set; } // Включен модуль "Менеджер задач"?
        public bool EnableAlwaysGPS { get; set; } // Включить поумолчанию рубильник "отправлять GPS"?
    }

/* СПРАВОЧНИКИ */

    /* Виды работ */
    [Table("Jobs")]
    public class Jobs
    {
        [PrimaryKey]
        public int Id { get; set; } // id вида работ
        public string Name { get; set; } // Название вида работ
    }

    /* Площадки */
    [Table("Platforms")]
    public class Platforms
    {
        [PrimaryKey]
        public int Id { get; set; } // id цеха, Платформы
        public int ParentID { get; set; } // id родительской платформы
        public string Name { get; set; } // Название цеха, платформы
    }

    /* Типы задач */
    [Table("TaskType")]
    public class TaskType
    {
        [PrimaryKey]
        public int Id { get; set; } // id типа
        public string Name { get; set; } // Название типа задач
    }

    /* Статус NearMiss */
    [Table("NearMissStatus")]
    public class NearMissStatus
    {
        [PrimaryKey]
        public int Id { get; set; } // id 
        public string Name { get; set; } // Название статуса
    }

    /* Статус требования */
    [Table("DemandStatus")]
    public class DemandStatus
    {
        [PrimaryKey]
        public int Id { get; set; } // id 
        public string Name { get; set; } // Название статуса
    }

    /* Статус задач */
    [Table("TaskStatus")]
    public class TaskStatus
    {
        [PrimaryKey]
        public int Id { get; set; } // id 
        public string Name { get; set; } // Название статуса
    }

    /* Статус документа */
    [Table("DocumentStatus")]
    public class DocumentStatus
    {
        [PrimaryKey]
        public int Id { get; set; } // id 
        public string Name { get; set; } // Название статуса
    }

    /* Роли пользователей для НМ */
    [Table("Rols")]
    public class Rols
    {
        [PrimaryKey]
        public int Id { get; set; } // id роли
        public string Name { get; set; } // Название роли
    }

    /* Пользователи */
    [Table("Users")]
    public class Users
    {
        [PrimaryKey]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string FatherName { get; set; }
        public string Platforms { get; set; }
        public int Role { get; set; }
    }

/* МЕНЕДЖЕР ЗАДАЧ */

    /* Задачи */
    [Table("Tasks")]
    public class Tasks
    {
        [PrimaryKey]
        public int Id { get; set; } // ID 
        public int Platform { get; set; } // площадка
        public string Status { get; set; } // Статус задачи
        public string Name { get; set; } // Тема
        public string Description { get; set; } // Описание 
        public int Director { get; set; } // Постановщик задачи 
        public string DateRegistration { get; set; } // Дата создания задачи
        public string DateDeadline { get; set; } // Срок задачи
        public string DateFinished { get; set; } // Дата выполнения задачи
        public string DateChange { get; set; } // Дата изменения задачи
        public int Typen { get; set; } // Тип задачи 
        public int SubType { get; set; } // 0 - главная задача. 1 - подзадача.
        public int Bunch { get; set; } // Номер Bunch 
        public string ReasonTaskReject { get; set; } // Причина отклонния
        public string ReasonDisagree { get; set; } // Причина несогласования
        public bool SendMarker { get; set; } // Маркер отправки изменений на сервер
    }

    /* Чек-листы */
    [Table("Audits")]
    public class Audits
    {
        [PrimaryKey]
        public int Id { get; set; } // ID 
        public int TaskId { get; set; } // ID задачи
        public int Ehs { get; set; } // EHS
        public int Job { get; set; } //  Виды работ
        public string Demand { get; set; } // Текст требования
        public string Event { get; set; } // Тип требования по срокам 
        public int Status { get; set; } // Статус соответствия
        public int DocumentND { get; set; } // Название документа
        public string DatePlan { get; set; } // Плановый срок выполнения
        public string Actions { get; set; } // Найденные нессответствия
        public string DateFact { get; set; } // фактическая дата выполнения
        public int Responsible { get; set; } // исполнитель
        public string DateChange { get; set; } // Дата проставления статуса требования
        public string Photo { get; set; } // Фото
        public bool SendMarker { get; set; } // Маркер отправки изменений на сервер
    }

    /* Реестр документов */
    [Table("Documents")]
    public class Documents
    {
        [PrimaryKey]
        public int ID { get; set; } // ID 
        public int ND { get; set; } // ND документа
        public string Name { get; set; } // Название
        public string Type { get; set; } // Тип документа
        public string Date { get; set; } // Дата введения и номер изменения
        public string Comment { get; set; } // Примечание документа
        public int Status { get; set; } // Статус документа
        public string Group { get; set; } // Группа документа
    }

}
