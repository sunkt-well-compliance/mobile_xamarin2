﻿using Xamarin.Forms;
using WellCompliance.Droid;
using Xamarin.Forms.Platform.Android;
using Android.Graphics.Drawables;
using WellCompliance.Renderer;

[assembly: ExportRenderer(typeof(XEdit), typeof(XEditRenderer))]
namespace WellCompliance.Droid
{
    public class XEditRenderer : EditorRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.Background = new ColorDrawable(Android.Graphics.Color.Transparent);
            }
        }
    }
}