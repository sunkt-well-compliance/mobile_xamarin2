﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WellCompliance2.Model
{
    class Jobs
    {
        SQL.Request request = new SQL.Request();
        /* Получить название вида работ, по его ID */
        public string GetJobNameFromId(int JobId)
        {
            string jobname = "Нет";
            string query;
            var connecting = request.BD();
            query = "SELECT `JobsName` FROM `Jobs` WHERE `Serverid`= " + JobId;
            var DataToBase = connecting.Query<SQL.Jobs>(query);
            

            foreach (var s in DataToBase)
            {
                jobname = s.Name;
            }
            return jobname;
        }
    }
}
