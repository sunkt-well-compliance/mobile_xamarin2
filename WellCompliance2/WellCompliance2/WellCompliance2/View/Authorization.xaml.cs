﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WellCompliance2.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Authorization : ContentPage
    {
        Model.Notification notification = new Model.Notification();
        public Authorization()
        {
            InitializeComponent();
        }

        /* Показать/спрятать пароль */
        void ClickedButtonSeePassword(object sender, EventArgs e)
        {
            if (PasswordInput.IsPassword == true)
            {
                ButtonSeePassword.Source = "IcoEyeOpen.png";
                PasswordInput.IsPassword = false;
            }
            else 
            {
                ButtonSeePassword.Source = "IcoEyeClose.png";
                PasswordInput.IsPassword = true;
            }

        }

        /* Действие при нажатии на кнопку ВОЙТИ */
        public async void ClickedButtonAuth(object sender, EventArgs e)
        {


            /* Если логин или пароль пустые */
            if ((LoginInput.Text == null) || (PasswordInput.Text == null))
            {
              //  await Navigation.PushAsync(new MyPopupPage());
               // notification.DisplayMessage(
               //     "Авторизация",
              //      "Логин или пароль не могут быть пустыми",
              //      "Ok");
               // Auth_button.IsEnabled = true;
            }
        }


    }
}