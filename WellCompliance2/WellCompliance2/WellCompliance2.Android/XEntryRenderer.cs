﻿using Xamarin.Forms;
using WellCompliance.Droid;
using Xamarin.Forms.Platform.Android;
using Android.Graphics.Drawables;
using WellCompliance.Renderer;

[assembly: ExportRenderer(typeof(XEntry), typeof(XEntryRenderer))]
namespace WellCompliance.Droid
{
    public class XEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.Background = new ColorDrawable(Android.Graphics.Color.Transparent);
            }
        }
    }
}